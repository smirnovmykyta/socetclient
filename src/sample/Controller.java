package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.awt.*;
import java.io.IOException;
import sample.Socket;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Button connect; // Value injected by FXMLLoader

    @FXML
    void action(ActionEvent event) throws IOException {
        setCommandStart();
    }

    public void setCommandStart() throws IOException {
        Socket socket = new Socket();
        socket.setCommandStart();
        socket.getDataSocket();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
