package sample;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class Socket {
    private SocketChannel channel;
    private SocketAddress socketAddress;

    private ByteBuffer byteBuffer;

    public void setCommandStart() throws IOException {
        channel = SocketChannel.open();
        socketAddress = new InetSocketAddress("localhost",9000);
        channel.connect(socketAddress);

        //CharBuffer charBuffer = CharBuffer.allocate(1024);
        //   charBuffer.append("HelloFromFX");
        byteBuffer = ByteBuffer.allocate(1024);

        ByteBuffer buf = ByteBuffer.wrap("HelloFromFX".getBytes(StandardCharsets.UTF_8));



        while (channel.write(buf) > 0 ){
            System.out.println("We have gone");
        }

    }

    public void getDataSocket(){

        String command = null;
        try {
            while (command == null) {
                channel.read(byteBuffer);
                byteBuffer.flip();

                command = new String(byteBuffer.array(), StandardCharsets.UTF_8);

                byteBuffer.clear();

                System.out.println(command);
            }
        }
        catch (Exception e){
            e.fillInStackTrace();
        }
    }
}
